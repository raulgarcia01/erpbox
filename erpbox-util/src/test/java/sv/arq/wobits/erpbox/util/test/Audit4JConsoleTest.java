/*
 * Copyright 2015 WOBITS.
 * World of Best IT Solutions - El Salvador.
 *
 * Este programa es de uso exclusivo para de la empresa WOBITS
 * y se encuentra protegido por las leyes de derechos de autor de la
 * Republica de El Salvador, se prohibe la copia y distribucion
 * total o parcial del mismo.
 *
 *      http://www.wobits.sv/licenses/LICENSE-1.0
 *
 */
package sv.arq.wobits.erpbox.util.test;

import org.audit4j.core.AuditManager;
import org.audit4j.core.dto.EventBuilder;
import org.junit.Test;
import sv.arq.wobits.erpbox.util.audit.ConfigurationERP;

/**
 * Clase que ejecuta prueba basica de Audit4J.
 *
 * @author raul.garcia
 */
public class Audit4JConsoleTest {

    /**
     * Constructor de la clase.
     */
    public Audit4JConsoleTest() {
    }

    /**
     * Ejecuta Audit4J con la MetaData modificada.
     */
    @Test
    public void consoleTest() {
        ConfigurationERP config = new ConfigurationERP();
        AuditManager manager = AuditManager.startWithConfiguration(config.getDefault());
        EventBuilder builder = new EventBuilder();
        builder.addActor("Container");
        builder.addOrigin("IP: 127.0.0.1 - consoleTest()");
        builder.addAction("Test");
        builder.addField("None", "Some Information");
        manager.audit(builder.build());
    }
}
