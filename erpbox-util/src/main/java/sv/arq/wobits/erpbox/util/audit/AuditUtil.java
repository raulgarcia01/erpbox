/*
 * Copyright 2015 WOBITS.
 * World of Best IT Solutions - El Salvador.
 *
 * Este programa es de uso exclusivo para de la empresa WOBITS
 * y se encuentra protegido por las leyes de derechos de autor de la
 * Republica de El Salvador, se prohibe la copia y distribucion
 * total o parcial del mismo.
 *
 *      http://www.wobits.sv/licenses/LICENSE-1.0
 *
 */
package sv.arq.wobits.erpbox.util.audit;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import org.audit4j.core.AuditManager;
import org.audit4j.core.dto.EventBuilder;

/**
 * Clase utilitaria que maneja el Audit4J en la aplicacion.
 *
 * @author raul.garcia
 */
public class AuditUtil implements Serializable {

    /**
     * Serial de la clase.
     */
    private static final long serialVersionUID = -3682303802787674676L;

    /**
     * Variable con el manager de auditoria.
     */
    private final AuditManager manager;

    /**
     * Variable de eventos de auditoria.
     */
    private EventBuilder buider;

    /**
     * Valor con el actor por defecto de la aplicacion.
     */
    private static final String DEFAULT_ACTOR = "[Container]";

    /**
     * Valor con el origen por defecto de la aplicacion.
     */
    private static final String DEFAULT_ORIGIN = "[127.0.0.1]";

    /**
     * Constructor de la Clase.
     */
    public AuditUtil() {
        ConfigurationERP config = new ConfigurationERP();
        this.buider = new EventBuilder();
        this.manager = AuditManager.startWithConfiguration(config.getDefault());
    }

    /**
     * Agrega el actor relacionado con la auditoria.
     *
     * @param actor Actor
     */
    public void addActor(String actor) {
        if ((actor == null) || (actor.equals(""))) {
            actor = this.DEFAULT_ACTOR;
        }
        buider.addActor(actor);
    }

    /**
     * Agrega el origen de la ejecucion en la auditoria.
     *
     * @param dirIP Direccion IP
     * @param city Ciudad
     */
    public void addOrigin(String dirIP, String city) {
        StringBuilder sb = new StringBuilder();
        if (((dirIP == null) || (dirIP.equals(""))) && ((city == null) || (city.equals("")))) {
            sb.append(this.DEFAULT_ORIGIN);
        } else {
            sb.append("|");
            sb.append(dirIP);
            sb.append("|");
            sb.append(city);
        }
        buider.addOrigin(sb.toString());
    }

    /**
     * Agrega la accion ejecutada por el actor.
     *
     * @param action
     */
    public void addAction(String action) {
        buider.addAction(action);
    }

    /**
     * Agrega la clave llave valor de los campos a almacenar.
     *
     * @param fields Campos
     */
    public void addFields(HashMap<String, Object> fields) {
        for (Map.Entry<String, Object> entry : fields.entrySet()) {
            buider.addField(entry.getKey(), entry.getValue());
        }
    }

    /**
     * Almacena el registro de auditoria.
     */
    public void saveAudit() {
        manager.audit(buider.build());
    }

}
