/*
 * Copyright 2015 WOBITS.
 * World of Best IT Solutions - El Salvador.
 *
 * Este programa es de uso exclusivo para de la empresa WOBITS
 * y se encuentra protegido por las leyes de derechos de autor de la
 * Republica de El Salvador, se prohibe la copia y distribucion
 * total o parcial del mismo.
 *
 *      http://www.wobits.sv/licenses/LICENSE-1.0
 *
 */
package sv.arq.wobits.erpbox.util.audit;

import java.io.Serializable;
import java.util.ResourceBundle;
import org.audit4j.core.Configuration;
import org.audit4j.core.handler.ConsoleAuditHandler;
import org.audit4j.core.handler.file.FileAuditHandler;
import org.audit4j.core.layout.SimpleLayout;
import org.audit4j.handler.db.DatabaseAuditHandler;

/**
 * Clase de archivo de configuracion de la libreria Audit4J.
 *
 * @author raul.garcia
 */
public class ConfigurationERP implements Serializable {

    /**
     * Serial de la clase
     */
    private static final long serialVersionUID = 4227444126663724488L;

    /**
     * Obtiene las propiedades del Audit4J.
     */
    private final ResourceBundle audit4jProp;

    /**
     * Constructor de la clase.
     */
    public ConfigurationERP() {
        this.audit4jProp = ResourceBundle.getBundle("audit4j");
    }

    /**
     * Retorna una configuracion customizada de Audit4J.
     *
     * @return Configuracion de Audit4J
     */
    public Configuration getDefault() {
        SimpleLayout simpleLayout = new SimpleLayout();
        simpleLayout.setDateFormat(audit4jProp.getString("date.format"));
        
        DatabaseAuditHandler dbHandler = new DatabaseAuditHandler();
        dbHandler.setEmbedded(audit4jProp.getString("db.embeded"));
        dbHandler.setDb_connection_type(audit4jProp.getString("db.connection"));
        dbHandler.setDb_driver(audit4jProp.getString("db.driver"));
        dbHandler.setDb_url(audit4jProp.getString("db.url"));
        dbHandler.setDb_user(audit4jProp.getString("db.user"));
        dbHandler.setDb_password(audit4jProp.getString("db.password"));
        
        Configuration config = new Configuration();
        config.addHandler(new ConsoleAuditHandler());
        config.addHandler(new FileAuditHandler());
        config.addHandler(dbHandler);
        config.setLayout(simpleLayout);
        config.addProperty("log.file.location", audit4jProp.getString("file.location"));
        return config;
    }
    
}
