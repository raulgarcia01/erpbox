/*
 * Copyright 2015 WOBITS.
 * World of Best IT Solutions - El Salvador.
 *
 * Este programa es de uso exclusivo para de la empresa WOBITS
 * y se encuentra protegido por las leyes de derechos de autor de la
 * Republica de El Salvador, se prohibe la copia y distribucion
 * total o parcial del mismo.
 *
 *      http://www.wobits.sv/licenses/LICENSE-1.0
 *
 */
package sv.arq.wobits.erpbox.queue;

import java.io.File;
import java.util.HashMap;
import java.util.Properties;
import javax.ejb.embeddable.EJBContainer;
import javax.naming.Context;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Clase que realiza un test a la cola de auditoria.
 *
 * @author raul.garcia
 */
public class AuditMessageSenderTest {

    /**
     * Variable que maneja el contexto EJB.
     */
    private static Context ctx;
    /**
     * Variable que maneja el contenedor EJB.
     */
    private static EJBContainer ejbContainer;

    /**
     * Parametros de la Clase.
     */
    private final HashMap<String, Object> parameters = new HashMap<>();

    /**
     * Constructor de la clase.
     */
    public AuditMessageSenderTest() {
    }

    /**
     * Apertura y configura el contenedor EJB.
     */
    @BeforeClass
    public static void setUpClass() {
        Properties properties = new Properties();
        properties.put("org.glassfish.ejb.embedded.glassfish.instance.reuse", Boolean.TRUE);
        File javaDir = new File("target/classes");
        File testDir = new File("target/test-classes");
        File[] modules = new File[]{javaDir, testDir};
        properties.put(EJBContainer.MODULES, modules);
        ejbContainer = EJBContainer.createEJBContainer(properties);
        System.out.println("Abriendo el contenedor");
        ctx = ejbContainer.getContext();
    }

    /**
     * Cierra el contenedor EJB.
     */
    @AfterClass
    public static void tearDownClass() {
        ejbContainer.close();
        System.out.println("Cerrado el contenedor");
    }

    /**
     * Test of enviarCola method, of class AuditMessageSender.
     */
    @Test
    public void testEnviarCola() throws Exception {
        System.out.println("enviarCola");
        HashMap<String, Object> info = new HashMap<>();
        info.put("customer", "Raul Garcia");
        AuditMessageDTO auditMessage = new AuditMessageDTO("raul.garcia", "addCustomer", info);
        AuditMessageSenderLocal instance = (AuditMessageSenderLocal) ctx.lookup("java:global/classes/AuditMessageSender");
        instance.enviarCola(auditMessage);
    }

}
