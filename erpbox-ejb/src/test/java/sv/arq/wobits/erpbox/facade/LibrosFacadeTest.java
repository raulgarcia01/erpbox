/*
 * Copyright 2015 WOBITS.
 * World of Best IT Solutions - El Salvador.
 *
 * Este programa es de uso exclusivo para de la empresa WOBITS
 * y se encuentra protegido por las leyes de derechos de autor de la
 * Republica de El Salvador, se prohibe la copia y distribucion
 * total o parcial del mismo.
 *
 *      http://www.wobits.sv/licenses/LICENSE-1.0
 *
 */
package sv.arq.wobits.erpbox.facade;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.Properties;
import javax.ejb.embeddable.EJBContainer;
import javax.naming.Context;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import sv.arq.wobits.erpbox.enums.TipoLibros;
import sv.arq.wobits.erpbox.model.Libros;

/**
 *
 * @author raul.garcia
 */
public class LibrosFacadeTest {

    /**
     * Variable que maneja el contexto EJB.
     */
    private static Context ctx;
    /**
     * Variable que maneja el contenedor EJB.
     */
    private static EJBContainer ejbContainer;

    /**
     * Parametros de la Clase.
     */
    private final HashMap<String, Object> parameters = new HashMap<>();

    /**
     * Constructor de la clase.
     */
    public LibrosFacadeTest() {
    }

    /**
     * Apertura y configura el contenedor EJB.
     */
    @BeforeClass
    public static void setUpClass() {
        Properties properties = new Properties();
        properties.put("org.glassfish.ejb.embedded.glassfish.instance.reuse", Boolean.TRUE);
        File javaDir = new File("target/classes");
        File testDir = new File("target/test-classes");
        File[] modules = new File[]{javaDir, testDir};
        properties.put(EJBContainer.MODULES, modules);
        ejbContainer = EJBContainer.createEJBContainer(properties);
        System.out.println("Abriendo el contenedor");
        ctx = ejbContainer.getContext();
    }

    /**
     * Cierra el contenedor EJB.
     */
    @AfterClass
    public static void tearDownClass() {
        ejbContainer.close();
        System.out.println("Cerrado el contenedor");
    }

    /**
     * Test of create method, of class LibrosFacade.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testCreate() throws Exception {
        System.out.println("create");
        
        Libros entity = new Libros();
        entity.setNombre("El Psiconalalista");
        entity.setDescripcion("Triller de Suspenso");
        entity.setFechaImpresion(new Date());
        entity.setPrecio(15.75);
        entity.setTipo(TipoLibros.TERROR);
        entity.setIsbn("01-5114-1415");
        
        LibrosFacadeLocal instance = (LibrosFacadeLocal) ctx.lookup("java:global/classes/LibrosFacade");
        instance.create(entity);
        
        String isbn = "01-5114-1415";
        parameters.clear();
        parameters.put(Libros.PARAM_ISBN, isbn);
        Libros libro = instance.findOneResult(Libros.FIND_BY_ISBN, parameters);
        assertNotNull(libro);
    }
    
}
