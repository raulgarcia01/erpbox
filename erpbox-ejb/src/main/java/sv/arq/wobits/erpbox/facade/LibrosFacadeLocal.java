/*
 * Copyright 2015 WOBITS.
 * World of Best IT Solutions - El Salvador.
 *
 * Este programa es de uso exclusivo para de la empresa WOBITS
 * y se encuentra protegido por las leyes de derechos de autor de la
 * Republica de El Salvador, se prohibe la copia y distribucion
 * total o parcial del mismo.
 *
 *      http://www.wobits.sv/licenses/LICENSE-1.0
 *
 */
package sv.arq.wobits.erpbox.facade;

import java.util.List;
import java.util.Map;
import javax.ejb.Local;
import sv.arq.wobits.erpbox.model.Libros;

/**
 *
 * @author raul.garcia
 */
@Local
public interface LibrosFacadeLocal {

    /**
     * Metodo que creat la entidad Libro
     *
     * @param entity Entidad Libro
     */
    void create(Libros entity);

    /**
     * Metodo que actualiza la entidad Libro
     *
     * @param entity Entidad Libro
     */
    void update(Libros entity);

    /**
     * Metodo que elimina la entidad Libro
     *
     * @param entity Entidad Libro
     */
    void delete(Libros entity);

    /**
     * Metodo que devuelve la entidad Libro por el Id
     *
     * @param id Identificador del Libro
     * @return Un Libro
     */
    Libros findById(Object id);

    /**
     * Metodo que devuelve todos los Libros.
     *
     * @return Lista de Libros
     */
    List<Libros> findAll();

    /**
     * Metodo que devuelve un Libro segun el NamedQuery.
     *
     * @param namedQuery Nombre del NamedQuery
     * @param parameters Lista de parametros
     * @return Lista de Libros
     */
    Libros findOneResult(String namedQuery, Map<String, Object> parameters);

    /**
     * Metodo que devuelve lista de Libros segun el NamedQuery.
     *
     * @param namedQuery Nombre del NamedQuery
     * @param parameters Lista de parametros
     * @return Lista de Libros
     */
    List<Libros> findListResult(String namedQuery, Map<String, Object> parameters);
}
