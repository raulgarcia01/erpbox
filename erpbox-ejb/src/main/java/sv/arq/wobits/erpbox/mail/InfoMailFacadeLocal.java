/*
 * Copyright 2015 WOBITS.
 * World of Best IT Solutions - El Salvador.
 *
 * Este programa es de uso exclusivo para de la empresa WOBITS
 * y se encuentra protegido por las leyes de derechos de autor de la
 * Republica de El Salvador, se prohibe la copia y distribucion
 * total o parcial del mismo.
 *
 *      http://www.wobits.sv/licenses/LICENSE-1.0
 *
 */
package sv.arq.wobits.erpbox.mail;

import javax.ejb.Local;

/**
 *
 * @author raul.garcia
 */
@Local
public interface InfoMailFacadeLocal {

    /**
     * Metodo que realiza el envio del email con copia.
     *
     * @param mailBody Cuerpo del Email
     */
    void sendMessageCC(MailBody mailBody);

    /**
     * Metodo que realiza el envio del email.
     *
     * @param mailBody Cuerpo del Email
     */
    void sendMessageSimple(MailBody mailBody);

}
