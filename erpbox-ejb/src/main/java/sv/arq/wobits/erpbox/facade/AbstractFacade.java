/*
 * Copyright 2015 WOBITS.
 * World of Best IT Solutions - El Salvador.
 *
 * Este programa es de uso exclusivo para de la empresa WOBITS
 * y se encuentra protegido por las leyes de derechos de autor de la
 * Republica de El Salvador, se prohibe la copia y distribucion
 * total o parcial del mismo.
 *
 *      http://www.wobits.sv/licenses/LICENSE-1.0
 *
 */
package sv.arq.wobits.erpbox.facade;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 *
 * @author raul.garcia
 * @param <T> Clase entidad
 */
public abstract class AbstractFacade<T> implements Serializable {

    /**
     * Serial de la clase.
     */
    private static final long serialVersionUID = -5135583225110086784L;

    /**
     * Clase de tipo entidad.
     */
    private final Class<T> entityClass;

    /**
     * Maneja el Log de la Clase.
     */
    private static final Logger logger = Logger.getLogger(AbstractFacade.class.getName());

    /**
     * Constructor de la Clase.
     *
     * @param entityClass Clase entidad
     */
    public AbstractFacade(Class<T> entityClass) {
        this.entityClass = entityClass;
    }

    /**
     * Inyecta el Entity Manager
     *
     * @return Entity Manager
     */
    protected abstract EntityManager getEntityManager();

    /**
     * Obtiene un registro de la clase entidad por la llave del objeto.
     *
     * @param id Llave del objeto
     * @return Objeto de la clase entidad
     */
    public final T findById(final Object id) {
        T result = null;
        try {
            result = getEntityManager().find(entityClass, id);
        } catch (Exception e) {
            logger.log(Level.SEVERE, null, e);
        }
        return result;
    }

    /**
     * Obtiene un listado de todos los elementos de la clase entidad.
     *
     * @return Listado de la clase entidad
     */
    public final List<T> findAll() {
        List<T> results = null;
        try {
            String query = "FROM " + entityClass.getSimpleName() + " e";
            results = getEntityManager().createQuery(query).getResultList();
        } catch (Exception e) {
            logger.log(Level.SEVERE, null, e);
        }
        return results;
    }

    /**
     * Obtiene un registro de la clase entidad por la llave del objeto.
     *
     * @param namedQuery Nombre del NamedQuery
     * @param parameters Parametros del NamedQuery
     * @return Objeto de la clase entidad
     */
    public final T findOneResult(final String namedQuery, final Map<String, Object> parameters) {
        T result = null;
        try {
            Query query = getEntityManager().createNamedQuery(namedQuery);
            if (parameters != null && parameters.size() > 0) {
                queryParameters(query, parameters);
            }
            result = (T) query.getSingleResult();
        } catch (Exception e) {
            logger.log(Level.SEVERE, null, e);
        }
        return result;
    }

    /**
     * Obtiene un listado de todos los elementos de la clase entidad por
     * NamedQuery definidos.
     *
     * @param namedQuery Nombre del NamedQuery
     * @param parameters Parametros del NamedQuery
     * @return Listado de la clase entidad
     */
    public final List<T> findListResult(final String namedQuery, final Map<String, Object> parameters) {
        List<T> results = null;
        try {
            Query query = getEntityManager().createNamedQuery(namedQuery);
            if (parameters != null && parameters.size() > 0) {
                queryParameters(query, parameters);
            }
            results = (List<T>) query.getResultList();
        } catch (Exception e) {
            logger.log(Level.SEVERE, null, e);
        }
        return results;
    }

    /**
     * Registra y prepara los parametros para los NamedQuerys de las entidades.
     *
     */
    private void queryParameters(final Query query, final Map<String, Object> parameters) {
        for (Entry<String, Object> entry : parameters.entrySet()) {
            query.setParameter(entry.getKey(), entry.getValue());
        }
    }

    /**
     * Crea y almacena una clase entidad en la base de datos.
     *
     * @param entity Entidad a crear
     */
    public final void create(final T entity) {
        try {
            getEntityManager().persist(entity);
        } catch (Exception e) {
            logger.log(Level.SEVERE, null, e);
        }
    }

    /**
     * Actualiza y almacena una clase entidad en la base de datos.
     *
     * @param entity Entidad a actualizar
     */
    public final void update(final T entity) {
        try {
            getEntityManager().merge(entity);
        } catch (Exception e) {
            logger.log(Level.SEVERE, null, e);
        }
    }

    /**
     * Elimina una clase entidad en la base de datos.
     *
     * @param entity Entidad a eliminar
     */
    public final void delete(final T entity) {
        try {
            getEntityManager().remove(entity);
        } catch (Exception e) {
            logger.log(Level.SEVERE, null, e);
        }
    }

}
