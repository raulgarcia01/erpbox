/*
 * Copyright 2015 WOBITS.
 * World of Best IT Solutions - El Salvador.
 *
 * Este programa es de uso exclusivo para de la empresa WOBITS
 * y se encuentra protegido por las leyes de derechos de autor de la
 * Republica de El Salvador, se prohibe la copia y distribucion
 * total o parcial del mismo.
 *
 *      http://www.wobits.sv/licenses/LICENSE-1.0
 *
 */
package sv.arq.wobits.erpbox.queue;

import java.io.Serializable;
import java.util.HashMap;
import lombok.Getter;
import lombok.Setter;

/**
 * Clase predefinida como contenedor de mensajes de auditoria.
 *
 * @author raul.garcia
 */
public class AuditMessageDTO implements Serializable {

    /**
     * Serial de la clase.
     */
    private static final long serialVersionUID = -965317161519729985L;

    /**
     * Constructor de la clase.
     *
     * @param actor Actor
     * @param action Accion
     * @param fields Campos
     */
    public AuditMessageDTO(String actor, String action, HashMap<String, Object> fields) {
        this.actor = actor;
        this.action = action;
        this.fields = fields;
    }

    /**
     * Constructor de la clase.
     *
     * @param actor Actor
     * @param dirIP Direccion IP
     * @param city Ciudad
     * @param action Accion
     * @param fields Campos
     */
    public AuditMessageDTO(String actor, String dirIP, String city, String action, HashMap<String, Object> fields) {
        this.actor = actor;
        this.dirIP = dirIP;
        this.city = city;
        this.action = action;
        this.fields = fields;
    }

    /**
     * Nombre del Actor.
     */
    @Getter
    @Setter
    private String actor;

    /**
     * Direccion IP del Actor.
     */
    @Getter
    @Setter
    private String dirIP;

    /**
     * Ciudad del Actor.
     */
    @Getter
    @Setter
    private String city;

    /**
     * Accion ejecutada.
     */
    @Getter
    @Setter
    private String action;

    /**
     * Campos relacionados.
     */
    @Getter
    @Setter
    private HashMap<String, Object> fields;
}
