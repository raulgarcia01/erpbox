/*
 * Copyright 2015 WOBITS.
 * World of Best IT Solutions - El Salvador.
 *
 * Este programa es de uso exclusivo para de la empresa WOBITS
 * y se encuentra protegido por las leyes de derechos de autor de la
 * Republica de El Salvador, se prohibe la copia y distribucion
 * total o parcial del mismo.
 *
 *      http://www.wobits.sv/licenses/LICENSE-1.0
 *
 */
package sv.arq.wobits.erpbox.model;

import java.io.Serializable;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Data;
import sv.arq.wobits.erpbox.enums.TipoLibros;

/**
 *
 * @author raul.garcia
 */
@Entity
@NamedQuery(name = Libros.FIND_BY_ISBN, query = "SELECT l FROM Libros l WHERE l.isbn =:" + Libros.PARAM_ISBN)
public @Data
class Libros implements Serializable, Cloneable {

    /**
     * Serial de la clase.
     */
    private static final long serialVersionUID = -572069513673159434L;

    /**
     * Nombre NamedQuery por busqueda por ISBN.
     */
    public static final String FIND_BY_ISBN = "findByISBN";
    /**
     * Nombre NamedQuery por busqueda por ISBN.
     */
    public static final String PARAM_ISBN = "paramISBN";

    /**
     * Identificador de la Entidad.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    /**
     * Nombre del Libro.
     */
    @NotNull(message = "{mensaje.notNull}")
    private String nombre;
    /**
     * Descripcion del Libro.
     */
    @Size(min = 5, max = 150, message = "{mensaje.sizeText}")
    private String descripcion;
    /**
     * Precio del Libro.
     */
    @Min(value = 0, message = "{mensaje.minVal}")
    private Double precio;
    /**
     * ISBN del Libro.
     */
    @NotNull(message = "{mensaje.notNull}")
    private String isbn;

    /**
     * Tipo del clasificacion de Libro.
     */
    @NotNull(message = "{mensaje.notNull}")
    private TipoLibros tipo;

    /**
     * Fecha de impresion del Libro.
     */
    @NotNull(message = "{mensaje.notNull}")
    @Temporal(TemporalType.DATE)
    private Date fechaImpresion;

    /**
     * Metodo que clona el objeto con sus valores.
     *
     * @return Libros Clon del objeto libro.
     */
    @Override
    public final Libros clone() {
        try {
            return (Libros) super.clone();
        } catch (CloneNotSupportedException e) {
            Logger.getLogger(Libros.class.getName()).log(Level.SEVERE, null, e);
            return null;
        }
    }

}
