/*
 * Copyright 2015 WOBITS.
 * World of Best IT Solutions - El Salvador.
 *
 * Este programa es de uso exclusivo para de la empresa WOBITS
 * y se encuentra protegido por las leyes de derechos de autor de la
 * Republica de El Salvador, se prohibe la copia y distribucion
 * total o parcial del mismo.
 *
 *      http://www.wobits.sv/licenses/LICENSE-1.0
 *
 */
package sv.arq.wobits.erpbox.queue;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.ejb.MessageDrivenContext;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import sv.arq.wobits.erpbox.util.audit.AuditUtil;

/**
 * Clase que recibe la cola de huellas de autotoria para su almacenamiento.
 *
 * @author raul.garcia
 */
@MessageDriven(activationConfig = {
    @ActivationConfigProperty(propertyName = "destinationLookup", propertyValue = "jms/AuditMessage"),
    @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue")
})
public class AuditMessageQueue implements MessageListener {

    /**
     * Maneja el contexto Message del JMS.
     */
    @Resource
    private transient MessageDrivenContext mdc;

    /**
     * Maneja el Log de la Clase.
     */
    private static final Logger logger = Logger.getLogger(AuditMessageQueue.class.getName());

    /**
     * Constructor de la clase.
     */
    public AuditMessageQueue() {
    }

    /**
     * Obtiene el valor del payload y transforma el mensaje a la entidad
     * contenedora AuditMessageDTO.
     *
     * @param message Payload
     */
    @Override
    public void onMessage(Message message) {
        ObjectMessage msg = null;
        try {
            if (message instanceof ObjectMessage) {
                msg = (ObjectMessage) message;
                AuditMessageDTO auditMsj = (AuditMessageDTO) msg.getObject();
                AuditUtil auditUtil = new AuditUtil();
                auditUtil.addActor(auditMsj.getActor());
                auditUtil.addAction(auditMsj.getAction());
                auditUtil.addOrigin(auditMsj.getDirIP(), auditMsj.getCity());
                auditUtil.addFields(auditMsj.getFields());
                auditUtil.saveAudit();
            }
        } catch (Exception e) {
            logger.log(Level.SEVERE, null, e);
            mdc.setRollbackOnly();
        }
    }

}
