/*
 * Copyright 2015 WOBITS.
 * World of Best IT Solutions - El Salvador.
 *
 * Este programa es de uso exclusivo para de la empresa WOBITS
 * y se encuentra protegido por las leyes de derechos de autor de la
 * Republica de El Salvador, se prohibe la copia y distribucion
 * total o parcial del mismo.
 *
 *      http://www.wobits.sv/licenses/LICENSE-1.0
 *
 */
package sv.arq.wobits.erpbox.queue;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.Session;

/**
 * Clase sender de los mensajes Queue de audotoria.
 *
 * @author raul.garcia
 */
@Stateless
public class AuditMessageSender implements AuditMessageSenderLocal {

    /**
     * Maneja el recurso del servidor MessageFactory.
     */
    @Resource(mappedName = "jms/AuditMessageFactory")
    private ConnectionFactory connectionFactory;

    /**
     * Maneja el nombre de la cola.
     */
    @Resource(mappedName = "jms/AuditMessage")
    private Queue queue;

    /**
     * Maneja el Log de la Clase.
     */
    private static final Logger logger = Logger.getLogger(AuditMessageSender.class.getName());

    @Override
    public void enviarCola(AuditMessageDTO auditMessage) {
        try {
            Connection connection = connectionFactory.createConnection();
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            MessageProducer messageProducer = session.createProducer((Destination) queue);

            ObjectMessage message = session.createObjectMessage();

            message.setObject(auditMessage);
            messageProducer.send(message);
            messageProducer.close();
        } catch (JMSException e) {
            logger.log(Level.SEVERE, null, e);
        }

    }
}
