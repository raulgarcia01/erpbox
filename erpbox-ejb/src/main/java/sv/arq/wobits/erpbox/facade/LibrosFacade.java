/*
 * Copyright 2015 WOBITS.
 * World of Best IT Solutions - El Salvador.
 *
 * Este programa es de uso exclusivo para de la empresa WOBITS
 * y se encuentra protegido por las leyes de derechos de autor de la
 * Republica de El Salvador, se prohibe la copia y distribucion
 * total o parcial del mismo.
 *
 *      http://www.wobits.sv/licenses/LICENSE-1.0
 *
 */
package sv.arq.wobits.erpbox.facade;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import sv.arq.wobits.erpbox.model.Libros;

/**
 *
 * @author raul.garcia
 */
@Stateless
public class LibrosFacade extends AbstractFacade<Libros> implements LibrosFacadeLocal {

    /**
     * Serial de la clase.
     */
    private static final long serialVersionUID = 862991294152733477L;

    /**
     * Injecta el Entity Manager.
     */
    @PersistenceContext(unitName = "erpbox-ejb-PU")
    private EntityManager em;

    /**
     * Constructor de la clase.
     */
    public LibrosFacade() {
        super(Libros.class);
    }

    /**
     * Injecta el Entity Manager a la clase Abstracta.
     * @return Entity Manager
     */
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
}
