/*
 * Copyright 2015 WOBITS.
 * World of Best IT Solutions - El Salvador.
 *
 * Este programa es de uso exclusivo para de la empresa WOBITS
 * y se encuentra protegido por las leyes de derechos de autor de la
 * Republica de El Salvador, se prohibe la copia y distribucion
 * total o parcial del mismo.
 *
 *      http://www.wobits.sv/licenses/LICENSE-1.0
 *
 */
package sv.arq.wobits.erpbox.queue;

import javax.ejb.Local;

/**
 * Interfaz que posee los metodos de envio de mensajes a la cola.
 *
 * @author raul.garcia
 */
@Local
public interface AuditMessageSenderLocal {

    /**
     * Metodo que envia el mensaje de auditoria a la cola.
     *
     * @param auditMessage Mensaje
     */
    void enviarCola(AuditMessageDTO auditMessage);
}
