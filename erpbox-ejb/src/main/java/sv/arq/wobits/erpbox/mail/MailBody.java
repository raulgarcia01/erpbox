/*
 * Copyright 2015 WOBITS.
 * World of Best IT Solutions - El Salvador.
 *
 * Este programa es de uso exclusivo para de la empresa WOBITS
 * y se encuentra protegido por las leyes de derechos de autor de la
 * Republica de El Salvador, se prohibe la copia y distribucion
 * total o parcial del mismo.
 *
 *      http://www.wobits.sv/licenses/LICENSE-1.0
 *
 */
package sv.arq.wobits.erpbox.mail;

import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;

/**
 * Clase que contiene la estructura de un Email.
 *
 * @author raul.garcia
 */
public class MailBody implements Serializable {

    /**
     * Serial de la clase.
     */
    private static final long serialVersionUID = -7768239559596077998L;

    /**
     * Constructor de la clase.
     */
    public MailBody() {
    }

    /**
     * Constructor de la clase.
     *
     * @param mailFrom Receptor
     * @param mailTo Destinatario
     * @param mailSubject Encabezado
     * @param mailMessage Mensaje
     */
    public MailBody(String mailFrom, String mailTo, String mailSubject, String mailMessage) {
        this.mailFrom = mailFrom;
        this.mailTo = mailTo;
        this.mailSubject = mailSubject;
        this.mailMessage = mailMessage;
    }

    /**
     * Constructor de la clase.
     *
     * @param mailFrom Receptor
     * @param mailTo Destinatario
     * @param mailCc Copia
     * @param mailSubject Encabezado
     * @param mailMessage Mensaje
     */
    public MailBody(String mailFrom, String mailTo, String mailCc, String mailSubject, String mailMessage) {
        this.mailFrom = mailFrom;
        this.mailTo = mailTo;
        this.mailCc = mailCc;
        this.mailSubject = mailSubject;
        this.mailMessage = mailMessage;
    }

    /**
     * Receptor del email.
     */
    @Getter
    @Setter
    private String mailFrom;

    /**
     * Destinatario del email.
     */
    @Getter
    @Setter
    private String mailTo;

    /**
     * Copia del email.
     */
    @Getter
    @Setter
    private String mailCc;

    /**
     * Encabezado del email.
     */
    @Getter
    @Setter
    private String mailSubject;

    /**
     * Mensaje del email.
     */
    @Getter
    @Setter
    private String mailMessage;

}
