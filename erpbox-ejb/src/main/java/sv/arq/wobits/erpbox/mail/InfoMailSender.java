/*
 * Copyright 2015 WOBITS.
 * World of Best IT Solutions - El Salvador.
 *
 * Este programa es de uso exclusivo para de la empresa WOBITS
 * y se encuentra protegido por las leyes de derechos de autor de la
 * Republica de El Salvador, se prohibe la copia y distribucion
 * total o parcial del mismo.
 *
 *      http://www.wobits.sv/licenses/LICENSE-1.0
 *
 */
package sv.arq.wobits.erpbox.mail;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.mail.Message;
import javax.mail.Message.RecipientType;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 * Clase utilitaria para el envia de Emails.
 *
 * @author raul.garcia
 */
@Stateless
public class InfoMailSender implements InfoMailFacadeLocal {

    /**
     * Maneja el recurso de Email.
     */
    @Resource(name = "mail/mhserver")
    private transient Session mailSession;

    /**
     * Maneja el Log de la Clase.
     */
    private static final Logger logger = Logger.getLogger(InfoMailSender.class.getName());

    @Override
    public void sendMessageSimple(MailBody mailBody) {
        Message msg = new MimeMessage(mailSession);
        try {
            msg.setFrom(new InternetAddress(mailBody.getMailFrom()));
            msg.setSubject(mailBody.getMailSubject());
            msg.setRecipient(RecipientType.TO,
                    new InternetAddress(mailBody.getMailTo()));
            msg.setText(mailBody.getMailMessage());
            Transport.send(msg);
        } catch (MessagingException ex) {
            logger.log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void sendMessageCC(MailBody mailBody) {
        Message msg = new MimeMessage(mailSession);
        try {
            msg.setFrom(new InternetAddress(mailBody.getMailFrom()));
            msg.setSubject(mailBody.getMailSubject());
            msg.setRecipient(RecipientType.TO,
                    new InternetAddress(mailBody.getMailTo()));
            msg.setRecipient(RecipientType.CC,
                    new InternetAddress(mailBody.getMailCc()));
            msg.setText(mailBody.getMailMessage());
            Transport.send(msg);
        } catch (MessagingException ex) {
            logger.log(Level.SEVERE, null, ex);
        }
    }

}
