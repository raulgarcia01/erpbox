/*
 * Copyright 2015 WOBITS.
 * World of Best IT Solutions - El Salvador.
 *
 * Este programa es de uso exclusivo para de la empresa WOBITS
 * y se encuentra protegido por las leyes de derechos de autor de la
 * Republica de El Salvador, se prohibe la copia y distribucion
 * total o parcial del mismo.
 *
 *      http://www.wobits.sv/licenses/LICENSE-1.0
 *
 */
package sv.arq.wobits.erpbox.enums;

/**
 *
 * @author raul.garcia
 */
public enum TipoLibros {

    /**
     * Referente al tipo Drama.
     */
    DRAMA("D"),
    /**
     * Referente al tipo Infantil.
     */
    INFANTIL("I"),
    /**
     * Referente al tipo Terror.
     */
    TERROR("T");

    /**
     * Código que representa el valor de Tipo Dios.
     */
    private String code;

    /**
     * Constructor de Tipo Dios.
     */
    private TipoLibros(final String code) {
        this.code = code;
    }

    /**
     * Retorna el valor del Tipo Dios.
     *
     * @return code
     */
    public String getCode() {
        return code;
    }

    /**
     * Setea el valor del Tipo Dios.
     *
     * @param code
     */
    public void setCode(final String code) {
        this.code = code;
    }

    /**
     * Obtiene la enumeración del Tipo Dios.
     *
     * @param code
     * @return tipoLibros
     */
    public static final TipoLibros getEnumCode(final String code) {
        for (TipoLibros row : values()) {
            if (row.getCode().equals(code)) {
                return row;
            }
        }
        return null;
    }
}
