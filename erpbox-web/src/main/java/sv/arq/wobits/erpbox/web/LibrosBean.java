/*
 * Copyright 2015 WOBITS.
 * World of Best IT Solutions - El Salvador.
 *
 * Este programa es de uso exclusivo para de la empresa WOBITS
 * y se encuentra protegido por las leyes de derechos de autor de la
 * Republica de El Salvador, se prohibe la copia y distribucion
 * total o parcial del mismo.
 *
 *      http://www.wobits.sv/licenses/LICENSE-1.0
 *
 */
package sv.arq.wobits.erpbox.web;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import lombok.Getter;
import sv.arq.wobits.erpbox.facade.LibrosFacadeLocal;
import sv.arq.wobits.erpbox.model.Libros;

/**
 *
 * @author raul.garcia
 */
@ManagedBean(name = "librosBean")
@ViewScoped
public class LibrosBean {

    /**
     * Injecta el servicio del EJB.
     */
    @EJB
    @Getter
    private LibrosFacadeLocal librosService;

    /**
     * Constructor de la clase.
     */
    public LibrosBean() {
    }

    /**
     * Devuelve la cantidad de Libros.
     * @return Cantidad de Libros.
     */
    public Integer countLibros() {
        List<Libros> lista = new ArrayList<>();
        lista.addAll(librosService.findAll());
        return lista.size();
    }

}
