/*
 * Copyright 2015 WOBITS.
 * World of Best IT Solutions - El Salvador.
 *
 * Este programa es de uso exclusivo para de la empresa WOBITS
 * y se encuentra protegido por las leyes de derechos de autor de la
 * Republica de El Salvador, se prohibe la copia y distribucion
 * total o parcial del mismo.
 *
 *      http://www.wobits.sv/licenses/LICENSE-1.0
 *
 */
package sv.arq.wobits.erpbox.web.audit;

import sv.arq.wobits.erpbox.web.*;

/**
 *
 * @author raul.garcia
 */
class ContextConfigParamsERP {

    /**
     * The Constant PARAM_HANDLERS.
     */
    static final String PARAM_HANDLERS = "handlers";

    /**
     * The Constant PARAM_LAYOUT.
     */
    static final String PARAM_LAYOUT = "layout";

    /**
     * The Constant PARAM_FILTERS.
     */
    static final String PARAM_FILTERS = "filters";

    /**
     * The Constant PARAM_OPTIONS.
     */
    static final String PARAM_OPTIONS = "options";

    /**
     * The Constant PARAM_META_DATA.
     */
    static final String PARAM_META_DATA = "metaData";

    /**
     * The Constant PARAM_PROPERTIES.
     */
    static final String PARAM_PROPERTIES = "properties";
}
