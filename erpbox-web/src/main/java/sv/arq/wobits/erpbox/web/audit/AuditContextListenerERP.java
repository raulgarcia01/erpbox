/*
 * Copyright 2015 WOBITS.
 * World of Best IT Solutions - El Salvador.
 *
 * Este programa es de uso exclusivo para de la empresa WOBITS
 * y se encuentra protegido por las leyes de derechos de autor de la
 * Republica de El Salvador, se prohibe la copia y distribucion
 * total o parcial del mismo.
 *
 *      http://www.wobits.sv/licenses/LICENSE-1.0
 *
 */
package sv.arq.wobits.erpbox.web.audit;

import javax.servlet.ServletContextEvent;
import org.audit4j.core.AuditManager;
import org.audit4j.core.web.AuditContextListener3;
import sv.arq.wobits.erpbox.util.audit.ConfigurationERP;

/**
 *
 * @author raul.garcia
 */
public class AuditContextListenerERP extends AuditContextListener3 {

    private ServletContexConfigSupportERP configSupport = null;

    @Override
    public void contextInitialized(ServletContextEvent contextEvent) {
        configSupport = new ServletContexConfigSupportERP();
        ConfigurationERP configurationERP = new ConfigurationERP();
        if (configSupport.hasHandlers(contextEvent.getServletContext())) {
            AuditManager.startWithConfiguration(configurationERP.getDefault());
        } else {
            AuditManager.startWithConfiguration(configurationERP.getDefault());
        }
    }

}
