/*
 * Copyright 2015 WOBITS.
 * World of Best IT Solutions - El Salvador.
 *
 * Este programa es de uso exclusivo para de la empresa WOBITS
 * y se encuentra protegido por las leyes de derechos de autor de la
 * Republica de El Salvador, se prohibe la copia y distribucion
 * total o parcial del mismo.
 *
 *      http://www.wobits.sv/licenses/LICENSE-1.0
 *
 */
package sv.arq.wobits.erpbox.web.audit;

import sv.arq.wobits.erpbox.web.*;
import javax.servlet.ServletContext;
import org.audit4j.core.Configuration;
import org.audit4j.core.MetaData;
import org.audit4j.core.filter.AuditEventFilter;
import org.audit4j.core.handler.Handler;
import org.audit4j.core.layout.Layout;
import org.audit4j.core.util.ReflectUtil;

/**
 *
 * @author raul.garcia
 */
public class ServletContexConfigSupportERP {

    Configuration loadConfig(ServletContext servletContext) {
        String handlers = servletContext.getInitParameter(ContextConfigParamsERP.PARAM_HANDLERS);
        String layout = servletContext.getInitParameter(ContextConfigParamsERP.PARAM_LAYOUT);
        String filters = servletContext.getInitParameter(ContextConfigParamsERP.PARAM_FILTERS);
        String options = servletContext.getInitParameter(ContextConfigParamsERP.PARAM_OPTIONS);
        String metaData = servletContext.getInitParameter(ContextConfigParamsERP.PARAM_META_DATA);
        String properties = servletContext.getInitParameter(ContextConfigParamsERP.PARAM_PROPERTIES);

        Configuration config = Configuration.INSTANCE;
        if (handlers != null && !handlers.equals("")) {
            config.setHandlers(new ReflectUtil<Handler>().getNewInstanceList(handlers.split(";")));
        }
        config.setLayout(new ReflectUtil<Layout>().getNewInstance(layout));
        if (filters != null && !filters.equals("")) {
            config.setFilters(new ReflectUtil<AuditEventFilter>().getNewInstanceList(filters.split(";")));
        }
        config.setOptions(options);
        config.setMetaData(new ReflectUtil<MetaData>().getNewInstance(metaData));
        if (properties != null && !properties.equals("")) {
            String[] propertiesList = properties.split(";");
            for (String property : propertiesList) {
                String[] keyValue = property.split(":");
                config.addProperty(keyValue[0], keyValue[1]);
            }
        }
        return config;
    }

    boolean hasHandlers(ServletContext servletContext) {
        String handlers = servletContext.getInitParameter("handlers");
        if (handlers == null || handlers.equals("")) {
            return false;
        } else {
            return true;
        }
    }

}
